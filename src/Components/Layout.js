import React from 'react';
import { MdOutlineShoppingCart } from "react-icons/md";
import { FaRegStar } from "react-icons/fa";
import { Link, Outlet  , } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Footer from "./Footer";
import DotaLogo from '../Images/dotalogo.jpg'
function Layout() {

    const selectorStar = useSelector(state => state.star.star)
    const selectOder = useSelector(state => state.oder.oder);
    const selectMassage = useSelector(state => state.formSlice.messageForm)
  
    return (
        <>
        <header>
                <Link to="/">
                    <img src={DotaLogo} alt="logo"/>
                </Link>
                <Link to="/">
                    <h1>DOTA ALL STARS SHOP</h1>
                </Link>
                <ul>
                    <span>{selectOder.length}</span>
                    <Link to="/Shop"> <MdOutlineShoppingCart className={`btn-star`}/></Link>
                    <span> {selectorStar.length}</span>
                    <Link to="/FavoriteStar"> <FaRegStar className={`btn-shop`}/></Link>
                </ul>

        </header>
            <main className="main">
                {
                    selectMassage &&
                    <div className="massage">You made a purchase </div>
                }


                <Outlet />
            </main>
            <Footer/>
        </>
    );
}

export { Layout};