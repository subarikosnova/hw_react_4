import React, { useState, useEffect } from 'react';
import Modal from "./Modal";
import { FaRegStar } from "react-icons/fa";
import PropTypes from 'prop-types';
import Button from './Button'
import { addToStar, deleteFromStar } from '../Assets/Slice/StarSlice';
import { useDispatch, useSelector } from 'react-redux';
import { addToBeforeOder } from "../Assets/Slice/AddOderSlice";
import { modalAccept } from '../Assets/Slice/AddModalSlice';



function CardItem({ itemCard, listMode }) {
  const [addStar, setAddStar] = useState(false)
  const dispatch = useDispatch()
  const selectorModalApprove = useSelector(state => state.isOpenApprove.isOpenApprove);


  const change = () => {
    setAddStar(!addStar)
  }

  useEffect(() => {
    const starData = JSON.parse(localStorage.getItem('Selectorstar'));

    if (starData) {
      starData.forEach((element) => {
        if (element.id === itemCard.id) {
          change();

        }
      });
    }
  }, []);

  return (
      <>
        <div className={`card ${addStar ? "star-active" : ""} ${listMode ? "list-mode" : ""}`}>
          <FaRegStar className={`star ${(addStar) && `star-active`}`} onClick={() => {
            change();
            !addStar ? dispatch(addToStar(itemCard)) : dispatch(deleteFromStar(itemCard));
          }}/>
          <img src={itemCard.image} alt="logo"/>
          <h2>{itemCard.name}</h2>
          <p>Article: {itemCard.article}</p>
          <p>Price: {itemCard.price} $</p>
          <p>Color {itemCard.color}</p>
          <Button clickAction={() => {
            dispatch(addToBeforeOder(itemCard))
            dispatch(modalAccept())
          }} Text={'Buy'}/>
        </div>
        {selectorModalApprove && <Modal Modal_text="Buy this item?"
                                        titleName="Want to buy?"
                                        isModalFor={`Approve`}
        />}
      </>
  );
}

CardItem.propTypes = {
  itemCard: PropTypes.object.isRequired,
}
export default CardItem






