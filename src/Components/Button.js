import React from 'react'

const Button = ({clickAction , Text}) => {

    return (
        <>
            {
                <button
                    onClick={() => {
                        clickAction()
                    }}>{Text}</button>
            }
        </>
    )
}

export default Button