import React from 'react'
import {Route, Routes} from 'react-router-dom';
import {ShoppingCart} from '../Pages/ShoppingCart';
import {FavoriteStar} from '../Pages/FavoriteStar';
import {Card} from "../Pages/Card";
import {Layout} from "./Layout";
import Form from "./Form";
import ContextProvider from './Context'


const AppRoutes = () => {
    return (
        <ContextProvider>
            <Routes>
                <Route path='/' element={<Layout />}>
                    <Route index path='/' element={<Card />} />
                    <Route path='/Shop' element={<ShoppingCart />} />
                    <Route path='/FavoriteStar' element={<FavoriteStar />} />
                    <Route path='/BuyForm' element={<Form />} />
                </Route>
            </Routes>
        </ContextProvider>
    );
}

export default AppRoutes