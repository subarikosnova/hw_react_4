import React, {useState, useEffect} from 'react'
import {MdDeleteOutline} from "react-icons/md";
import {FaRegStar} from "react-icons/fa";
import {odersDeleteFun} from '../Assets/Slice/OderDeleteSlice';
import {deleteFromStar, addToStar} from '../Assets/Slice/StarSlice';
import {useDispatch} from 'react-redux';
import {ToggleModalDelete} from "../Assets/Slice/ModalDeleteSlice";
import PropTypes from "prop-types";

const OrdersItem = ({item}) => {


    const [addStar, setaddStar] = useState(false)
    const dispatch = useDispatch()
    useEffect(() => {
        setTimeout(()=>{
            const starData = JSON.parse(localStorage.getItem('StarSelect'));
            if (starData) {
                starData.forEach((element) => {
                    if (element.id === item.id) {
                        change();
                        console.log(element)
                    }
                });
            }
        } , 0.1)
    }, []);

    const change = () => {
        setaddStar(!addStar)

    }

    useEffect(() => {
        const starData = JSON.parse(localStorage.getItem('StarSelect'));
        if (starData) {
            starData.forEach((element) => {
                if (element.idTime === item.idTime) {
                    change();
                }
            });
        }
    }, []);


    return (
        <div>
            {
                <div className="card">
                    <FaRegStar className={`star ${(addStar) && `star-active`}`} onClick={() => {
                        change();
                        !addStar ? dispatch(addToStar(item)) : dispatch(deleteFromStar(item))
                    }}/>
                    <img src={item.image} alt="logo"/>
                    <h2>{item.name}</h2>
                    <p>Article: {item.article}</p>
                    <p>Price: {item.price} $</p>
                    <p>Color {item.color}</p>
                    <MdDeleteOutline className='btn-trash' onClick={() => {
                        dispatch(odersDeleteFun(item))
                        dispatch(ToggleModalDelete())
                    }}/>
                </div>

            }
        </div>
    )
}
OrdersItem.propTypes = {
    item: PropTypes.object.isRequired,
}
export default OrdersItem