import { render, screen, fireEvent } from '@testing-library/react';
import Button from './Button'


describe('Button test' , () => {
    test('Button test, onClick function' , () => {
        const onClickEmulation = jest.fn();
        const Text = 'Buy';
        render(<Button clickAction={onClickEmulation} Text={Text}/>)
        const button = screen.getByText(Text)
        fireEvent.click(button)
        expect(onClickEmulation).toHaveBeenCalledTimes(1)
    })
    test("Snapshot", () => {
        // eslint-disable-next-line testing-library/render-result-naming-convention
        const button = render(<Button />);
        expect(button).toMatchSnapshot();
    });

})
