import  React, {useState, createContext } from "react";

export const MyContext = createContext();


const Context = ({children}) => {
    const [list , setList] = useState(false)
    const listDisable = () => {
        setList(false)
    }

    const listEnable = () => {
        setList(true)
    }


    return (
        <MyContext.Provider value={{ list, listEnable, listDisable }}>
            {children}
        </MyContext.Provider>
    )

};

export default Context;
