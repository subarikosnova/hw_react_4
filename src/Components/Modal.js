import React from 'react';
import PropTypes from 'prop-types';
import Button from './Button'
import {addOder , deleteFromOder } from '../Assets/Slice/OderSlice';
import {useSelector , useDispatch } from 'react-redux';
import {modalAccept } from '../Assets/Slice/AddModalSlice';
import {ToggleModalDelete} from "../Assets/Slice/ModalDeleteSlice";

function Modal({Modal_text, titleName, isModalFor}) {
    const selectorOrder = useSelector(state => state.beforOder.beforOder)
    const selectorDelete = useSelector(state => state.deleteOder.deleteOder)
    const dispatch = useDispatch();

    return (
        <>
            <div id="overlay" className="overlay" onClick={(e) => (
                e.target.id === "overlay" && (isModalFor === "Approve" ? dispatch(modalAccept()) :  dispatch(ToggleModalDelete()) )
            )}>

                <div className="modal-content">
                    <h2 className="modal-content_title">{titleName}</h2>
                    <p >{Modal_text}</p>

                   { isModalFor === "Approve" && <div className="modal__btn-wrapper">
                        <Button
                       clickAction={()=>{
                               dispatch(addOder(selectorOrder));
                               dispatch(modalAccept());
                       }} 
                    Text={'Ok'}
                         />
                       <Button clickAction={()=>{dispatch(modalAccept())}} Text={'Cansel'} />
                    </div>
                     }
                        { isModalFor === "Delete" && <div className="modal__btn-wrapper">
                        <Button
                            clickAction={()=>{
                        dispatch(deleteFromOder(selectorDelete))
                          dispatch(ToggleModalDelete())
                      }}   Text={'ok'}
                        />
                       <Button
                           clickAction={()=>{
                              dispatch(ToggleModalDelete())
                           }}
                           Text={'Cansel'}
                         />
                    </div>
                     }
                </div>
            </div>
        </>
    );
}
Modal.propTypes = {
    Modal_text: PropTypes.string,
    titleName: PropTypes.string,
}
export default Modal;