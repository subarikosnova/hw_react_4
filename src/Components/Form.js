import React from 'react';
import {useFormik} from "formik";
import {validationForm} from "./ValidationForm";
import {useDispatch} from 'react-redux';
import {deleteAllOders} from "../Assets/Slice/OderSlice";
import {MessageFormCreate} from "../Assets/Slice/MassageSlice";
import {useState} from "react";
import {useNavigate} from "react-router-dom";


const initialValues = {
    name: "",
    surname: "",
    age: "",
    address: "",
    mobile: ""
}

function Form() {
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onSubmitHandler = (values) => {
        setLoading(true);

        setTimeout(function() {
            dispatch(deleteAllOders());
            dispatch(MessageFormCreate());
            localStorage.removeItem('selectorOrders');
            navigate('/');
            setLoading(false);

            setTimeout(() => {
                dispatch(MessageFormCreate());
            }, 7000);
        }, 2000);
    };



    const {
        values,
        handleChange,
        handleSubmit,
        handleBlur,
        errors,
        touched} = useFormik({
            initialValues: initialValues,
            validationSchema: validationForm,
            onSubmit: onSubmitHandler
        }
    )


    return (
        <div>
            <form className="form" onSubmit={handleSubmit}>
                <label htmlFor="name">User Name</label>
                <input type="text" id="name" value={values.name} onBlur={handleBlur} onChange={handleChange}/>
                {touched.name && errors.name && <p>{errors.name}</p>}
                <label htmlFor="surname">Sur Name</label>
                <input type="text" id="surname" value={values.surname} onBlur={handleBlur} onChange={handleChange}/>
                {touched.surname && errors.surname && <p>{errors.surname}</p>}
                <label htmlFor="age">Age</label>
                <input type="text" id="age" value={values.age} onBlur={handleBlur} onChange={handleChange}/>
                {touched.age && errors.age && <p>{errors.age}</p>}
                <label htmlFor="address">Address</label>
                <input type="text" id="address" value={values.address} onBlur={handleBlur} onChange={handleChange}/>
                {touched.address && errors.address && <p>{errors.address}</p>}
                <label htmlFor="mobile">Mobile Number</label>
                <input placeholder="(___)___-__-__" type="text" id="mobile" value={values.mobile} onBlur={handleBlur}
                       onChange={handleChange}/>
                {touched.mobile && errors.mobile && <p>{errors.mobile}</p>}
                <button className="buy-btn" type="submit"  >Buy</button>
            </form>
            {
                loading && <div className="load"><hr/><hr/><hr/><hr/></div>
            }
        </div>
    );
}

export default Form;