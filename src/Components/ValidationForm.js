import * as yup from 'yup';

export const validationForm = yup.object({
    name: yup.string()
        .matches(/^[A-Za-zА-Яа-яЁёІіЇїЄєҐґ]+$/, "Ім'я повинне містити тільки літери")
        .required("Ім'я обов'язкове для заповнення"),
    surname: yup.string().min(3).required("Please Enter Your SurName"),
    age: yup.number().required("Please Enter Age"),
    address: yup.string().min(6).required("Please Enter Your Address"),
    mobile: yup.string()
        .matches(/^[0-9]{10}$/, "Телефон повинен бути у форматі (XXX) XXX-XXXX")
        .required("Телефон обов'язковий для заповнення"),
});