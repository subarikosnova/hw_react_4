import React from 'react';
import { render, screen } from '@testing-library/react';
import Modal from './Modal';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import '@testing-library/jest-dom';



test('Modal renders with correct text', () => {
    const modalText = 'Buy this item?';
    const titleName = 'Want to buy?';
    const mockStore = configureStore();
    const store = mockStore({
        beforOder: {
            beforOder:[] ,
        },
        deleteOder: {
            deleteOder: [],
        },
       
    });


    render(
        <Provider store={store}>
            <Modal Modal_text={modalText} titleName={titleName} />
        </Provider>
    );
   
    expect(screen.getByText(modalText)).toBeInTheDocument();

   
    expect(screen.getByText(titleName)).toBeInTheDocument();

   
    expect(<Modal/>).toMatchSnapshot();
});
