import CardItem from "../Components/CardItem";
import { useSelector } from "react-redux";
import {MyContext} from '../Components/Context'
import React, { useContext } from "react";
import { FaRegListAlt } from "react-icons/fa";
import { IoGridOutline } from "react-icons/io5";



function Card() {
    const selector = useSelector(state => state.item.item);
    const { listEnable, listDisable, list } = useContext(MyContext);


    const handleListEnable = () => {
        listEnable();
    };

    const handleListDisable = () => {
        listDisable();
    };
    return (

        <div className={`main ${list ? "list-enabled" : "list-disabled"}`}>
            <div className="type-btn">
                <FaRegListAlt className="btn-style" onClick={handleListDisable} />
                <IoGridOutline className="btn-style" onClick={handleListEnable} />
            </div>


            {selector.map((el) => (
                <CardItem key={el.id}
                    itemCard={el}
                   />
            ))
            }

        </div>
    );
}

export {Card};