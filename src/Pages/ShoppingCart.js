import OrdersItem from '../Components/OrdersItem';
import { useSelector} from 'react-redux';
import { Link } from 'react-router-dom';
import Modal from "../Components/Modal";
import React from "react";


function ShoppingCart() {
    const deleteModal = useSelector(state => state.isOpenDel.isModalDelOpen)
    const selectOder = useSelector(state => state.oder.oder);


    return (
        <>
            <div className='main'>

                {selectOder.length > 0 ?
                    selectOder.map((el) => (
                        <OrdersItem
                            key={el.id}
                            item={el}
                        />
                    )) : <span> Now is empty</span>}
            </div>

            <div className="form-btn">
            <Link  to="/BuyForm">
                <span className="buy-btn">Buy Now</span>
            </Link>
            </div>
            {deleteModal && <Modal
                Modal_text="Do you want delete this item from buy list?"
                titleName="Delete"
                isModalFor={"Delete"}
            />}
        </>
    );
}


export {ShoppingCart};