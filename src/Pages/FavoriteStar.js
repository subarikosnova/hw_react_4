import React from 'react';
import { FaRegStar } from "react-icons/fa";
import { useSelector , useDispatch } from 'react-redux';
import { deleteFromStar } from '../Assets/Slice/StarSlice';
function FavoriteStar() {
    const stars = useSelector(state => state.star.star)
    const dispatch = useDispatch()
    return (
        <div  className='main'>
            {
                stars.length > 0 ?
                stars.map((el)=>( 
                    <div className="card" key={el.id}>
                        <FaRegStar className='star' onClick={()=>{
                          dispatch(deleteFromStar(el))
                        }} />
                        <img src={el.image} alt="oders"/>
                        <h2>{el.name}</h2>
                        <p>color: {el.color}</p>
                        <b>{el.price} $</b>
                    </div>
                )) : <span> Now is empty</span>
            }

        </div>
    );
}

export {FavoriteStar};