import React, { useEffect } from "react";
import AppRoutes from './Components/AppRoutes';
import { useSelector, useDispatch } from 'react-redux';
import { effectStar } from './Assets/Slice/StarSlice';
import { effectOder } from './Assets/Slice/OderSlice';
import { fetchTodos } from './Assets/Slice/ItemSlice';



function App() {

    const dispatch = useDispatch()
    const selectorStar = useSelector(state => state.star.star)
    const selectorOrders = useSelector(state => state.oder.oder)


    useEffect(() => {
        dispatch(effectOder(JSON.parse(localStorage.getItem(`selectorOrders`)) || []))
    }, [])

    useEffect(() => {
        localStorage.setItem(`selectorOrders`, JSON.stringify(selectorOrders))
    }, [selectorOrders])


    useEffect(() => {
        dispatch(effectStar(JSON.parse(localStorage.getItem(`StarSelect`)) || []))
    }, [])

    useEffect(() => {
        localStorage.setItem(`StarSelect`, JSON.stringify(selectorStar))
    }, [selectorStar])
    useEffect(() => {

    dispatch(fetchTodos());
}, []);

    return (<>
        { <div className="App">
            <AppRoutes  />
        </div>}
    </>
    );
}


export default App;
