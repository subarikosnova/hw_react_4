import { configureStore } from '@reduxjs/toolkit';
import itemReducer from './Slice/ItemSlice';
import starListSlice from './Slice/StarSlice';
import BeforOder from "./Slice/AddOderSlice";
import Oder from "./Slice/OderSlice";
import deleteOder from './Slice/OderDeleteSlice';
import isOpenApprove from './Slice/AddModalSlice';
import ModalDelToogle from "./Slice/ModalDeleteSlice";
import MassageSlice from "./Slice/MassageSlice";




const store = configureStore({
    reducer: {
        item: itemReducer,
        star: starListSlice,
        beforOder: BeforOder,
        oder: Oder,
        deleteOder: deleteOder,
        isOpenApprove : isOpenApprove,
        isOpenDel : ModalDelToogle,
        formSlice: MassageSlice,

    },
});
export default store