import { createSlice , createAsyncThunk} from "@reduxjs/toolkit"

 export const fetchTodos = createAsyncThunk(
   'todos/fetchTodos',
   async function() {
     const response = await fetch(`/product.json`);
       const data = await response.json()
       return data.item
   }
 )
 const itemSlice = createSlice({
 name: `item` ,
 initialState: {
     item : []
 } , 
 reducers: {

  },
   extraReducers(builder) {
    builder
        .addCase(fetchTodos.pending, (state) => {
            state.status = 'loading';
            state.error = null;
        })
        .addCase(fetchTodos.fulfilled, (state, action) => {
            state.status = 'resolve';
             state.item = action.payload
        })
        .addCase(fetchTodos.rejected, (state, action) => {
            state.status = 'rejected';
        });
}

 })

 export default itemSlice.reducer;



