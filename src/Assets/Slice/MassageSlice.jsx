import {createSlice} from "@reduxjs/toolkit"


const MassageForm = createSlice({
        name: `star`,
        initialState: {
            messageForm: false
        }, reducers: {
            MessageFormCreate(state) {
                state.messageForm = !state.messageForm
            },
        }
    }
)

export const {MessageFormCreate} = MassageForm.actions;
export default MassageForm.reducer;