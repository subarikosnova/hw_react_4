import { createSlice } from "@reduxjs/toolkit"

const starSlice = createSlice({
name: `star` ,
initialState: {
    star : []
} , 
reducers: {
  addToStar(state ,action  ){
 state.star.push(action.payload  )

  },
  deleteFromStar(state , action){
    state.star = state.star.filter((el)=> el.id !== action.payload.id)
  } ,

    effectStar(state , action ){
state.star =  action.payload;

  }
}
})
export const {addToStar,deleteFromStar , effectStar } =  starSlice.actions;
export default starSlice.reducer;