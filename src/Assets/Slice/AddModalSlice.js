import { createSlice } from "@reduxjs/toolkit"

const AddModalSlice = createSlice( {
      name: `ModalAccept` ,
initialState: {
    isOpenApprove : false
}, 
reducers:{
    modalAccept(state , action){
        state.isOpenApprove = !state.isOpenApprove
    }
}
    })
export const {modalAccept} = AddModalSlice.actions
export default AddModalSlice.reducer