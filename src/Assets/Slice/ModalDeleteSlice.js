import {createSlice} from "@reduxjs/toolkit";

const ModalDeleteSlice = createSlice({
    name: `ModalDelete` ,
    initialState:{
        isModalDelOpen: false
    },
    reducers:{
        ToggleModalDelete(state){
            state.isModalDelOpen = !state.isModalDelOpen
        }
    }
})
export const {ToggleModalDelete} = ModalDeleteSlice.actions;
export default ModalDeleteSlice.reducer
