import { createSlice} from "@reduxjs/toolkit"

const oderSlice = createSlice({
    name: `Oder`,
    initialState: {
        oder: []
    },
    reducers: {

        addOder(state, action) {
            let approve = false
            state.oder.forEach((el)=>{
                if (el.id === action.payload.id) approve = true
            })
          !approve && state.oder.push({...action.payload ,  idTime :new Date().toISOString() })
         
        },
        deleteFromOder(state, action) {
            state.oder = state.oder.filter((el) => el.id !== action.payload.id)
     
        },
        effectOder(state, action) {
            state.oder = action.payload;
        },
        deleteAllOders(state) {
            state.oder = []
        },

    }
})
export const { addOder, deleteFromOder, effectOder, deleteAllOders } = oderSlice.actions;
export default oderSlice.reducer;