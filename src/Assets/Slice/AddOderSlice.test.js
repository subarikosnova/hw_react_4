import addOderReducer, { addToBeforeOder } from './AddOderSlice';

describe('addOderReducer', () => {
    it('should handle initial state', () => {
        const initialState = {
            beforeOder: [],
        };
        expect(addOderReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle addToBeforeOder action', () => {
        const initialState = {
            beforeOder: [],
        };

        const newData = [/* your test data here */];


        const nextState = addOderReducer(initialState, addToBeforeOder(newData));


        expect(nextState.beforeOder).toEqual(newData);
    });
});
