import AddModalReducer, { modalAccept } from './AddModalSlice';

describe('AddModalReducer', () => {
    it('should handle initial state', () => {
        const initialState = {
            isOpenApprove: false,
        };
        expect(AddModalReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle modalAccept action', () => {
        const initialState = {
            isOpenApprove: false,
        };

        const nextState = AddModalReducer(initialState, modalAccept());

        expect(nextState.isOpenApprove).toEqual(true);
    });

    it('should handle modalAccept action toggling', () => {
        const initialState = {
            isOpenApprove: false,
        };

        const nextState1 = AddModalReducer(initialState, modalAccept());
        const nextState2 = AddModalReducer(nextState1, modalAccept());

        expect(nextState1.isOpenApprove).toEqual(true);
        expect(nextState2.isOpenApprove).toEqual(false);
    });
});
